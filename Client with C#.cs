﻿using SimpleTCP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Collections;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Threading;

namespace TCP_Client
{
    public partial class Form1 : Form
    {
        System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
        
        

        public Form1()
        {
            InitializeComponent();
            
    }

        SimpleTcpClient client;
                           
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            btnConnect.Enabled = false;
                  
            clientSocket.Connect(txtHost.Text, int.Parse(txtPort.Text));
            
            NetworkStream serverStream = clientSocket.GetStream();
                                                       
        }

        byte[] Combine(byte[] a1 , byte[] a2)
        {
            byte[] ret = new byte[a1.Length + a2.Length];
            Array.Copy(a1, 0, ret, 0, a1.Length);
            Array.Copy(a2, 0, ret, a1.Length, a2.Length);
            return ret;
        }
     
        private void button2_Click(object sender, EventArgs e)
        {
            Boolean[] bitler = { true, false, true, true, false, false, true, true };
            BitArray bitDizisi = new BitArray(bitler);
           
            byte[] resultBytes = new byte[(bitDizisi.Length - 1) / 8 + 1];
            bitDizisi.CopyTo(resultBytes, 0);

            txtStatus.AppendText(txtMessage.Text + "\n");
            NetworkStream serverStream = clientSocket.GetStream();
            
            byte[] messageInBits = System.Text.Encoding.ASCII.GetBytes(txtMessage.Text);
            byte[] outStream = Combine(resultBytes, messageInBits);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();
            txtMessage.Text = String.Empty;
        }

        private byte[] Combine(object p, byte[] messageInBits)
        {
            throw new NotImplementedException();
        }

        private void txtPort_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            client = new SimpleTcpClient();
            client.StringEncoder = Encoding.UTF8;
            client.DataReceived += Client_DataReceived; 
        }

        private void Client_DataReceived(object sender, SimpleTCP.Message e)
        {
            txtStatus.Invoke((MethodInvoker)delegate ()
            {
                txtStatus.Text += e.MessageString;

            });
        }

        private void txtHost_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtStatus_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMessage_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
